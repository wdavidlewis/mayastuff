#
# MIT License
# 
# Copyright (c) 2007-2020 W David Lewis
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

'''
Maya tagging library for tagging and listing tagged objects.

Based on a discussion with Seth Gibson when we were at Crystal Dynamics and
pretty sure he got the idea from Jason Schleifer's post mentioned in the below
link.

Some more current, related info:
http://www.chrisevans3d.com/pub_blog/mighty-message-attribute/

Pretty much a clean-room re-implementation of what I wrote for Crystal in 2007.

Tags are represented by a Maya network node with two attributes: an integer
version and a multi attribute of messages.  All tags begin with 'tag_' in the
name.  Currently they are in the top-level namespace.

Connects the message attribute on object transforms to the multi attribute on the
tag node. I do it this way as there are no modifications (e.g. extra
attributes) to the tagged items.  This tries to keep with the idea that there's
pipeline data and there's asset data and they are separate. Delete the tag
nodes and the obects are pristine - no grotty cleanup like removing attributes
required.

Tag Implementation
In theory, all the implementation details are in these _ functions.
They should hide the implementation specifics.  Changing these functions
should be all you need to do to change the underlying mechanics.

'''
from pymel.all import PyNode, ls, objExists
from pymel.core.general import createNode, addAttr, setAttr, connectAttr, disconnectAttr, delete, isConnected

###############################################################################
# Implementation
###############################################################################
def _getTagNode(tagName, raiseIfTagDoesntExist=False):
    ''' Gets a tag node by name. Creates node if it doesn't exist unless
    raiseIfTagNodeDoesntExist is set.  In that case it will rais a Maya
    exception.
 
    Args:
       tagName (str,unicode):  The tag to find/create.

    kwargs:
        raiseIfTageDoesntExist - default value is False

    Raises:
       MayaNodeError - specified tag does not exist

    Returns:
       tag node as a PyNode.
   '''
    tagNodeName = 'tag_%s' % tagName
    if objExists(tagNodeName):
        return PyNode(tagNodeName)
    if raiseIfTagDoesntExist:
        raise ValueError("Specified tag <%s> doesn't exist" % tagName)
    n = createNode('network', n=tagNodeName, ss=True)
    if not n:
        raise RuntimeError("Could not create tag node %s" % tagNodeName)
    addAttr(n, shortName='ver', longName='version', attributeType='short')
    n.version.set(1)
    addAttr(n, shortName='con', longName='connection', attributeType='message', multi=True, im=False)
    return n

def _connectTag(tagNode, itemNode):
    ''' Connects a tag to a item.  Takes PyNodes.
 
    Args:
       tagNode (PyNode):  The tag to connect.
       itemNode (PyNode): The item.

    Raises:
       MayaNodeError - specified tag or item does not exist

    Returns:
       nothing.
   '''
    try:
        connectAttr(itemNode.message, tagNode.connection, na=True) 
    except RuntimeError:
        pass

def _disconnectTag(tagNode, itemNode):
    ''' Disonnects a tag to a item.  Takes PyNodes.
 
    Args:
       tagNode (PyNode):  The tag to connect.
       itemNode (PyNode): The item.

    Raises:
       MayaNodeError - specified tag or item does not exist

    Returns:
       nothing.
   '''
    #print (itemNode.longName())
    #print (tagNode.longName())
    disconnectAttr(itemNode.message, tagNode.connection, na=True) 

def _listTags():
    ''' Lists tags in the scene.
 
    Raises:
       MayaNodeError - specified tag or item does not exist

    TODO: Handle tags in references.

    Returns:
       nothing.
   '''
    tagList = []
    for n in ls(typ='network'):
        barenodename = str(n).split(':')[-1]
        if barenodename.startswith('tag_'):
            tagList.append(barenodename[4:])
    return tagList

def _listTagsOnSingleItem(item):
    ''' Lists tags associated with a single item.
 
    Args:
       item (str,PyNode): The item.

    Raises:
       MayaNodeError - specified tag or item does not exist

    TODO: Handle tags in references.

    Returns:
       nothing.
   '''
    if type(item) in [str]:
        item = PyNode(item)
    return {str(n).split(':')[-1][4:] for n in item.listConnections(t='network') if str(n).split(':')[-1].startswith('tag_')}

def _listItemsWithTag(tagName, unreferencedItemsOnly=False):
    ''' Lists the items with a single tag.
 
    Args:
       tagName (str):  The tag to list items on.

    kwargs:
        unreferencedItemsOnly (False):

    Raises:
       ValueError - specified tag does not exist

    Returns:
       nothing.
   '''
    tagNodeName = 'tag_%s' % tagName
    tags = ls(tagNodeName, r=True)
    if not tags:
        raise ValueError("Invalid tag <%s> supplied" % tagName)
    itemlist = set()
    for tag in tags:
        itemlist.update({n for n in PyNode(tag).connection.listConnections()})
    return itemlist

###############################################################################
# Tag public interface
###############################################################################
def tagItem(tagList, itemList):
    """Applies one or more tags to one or more items. Creates tags as required.

    Args:
       tagList (str,unicode,PyNode or [] of those):  The tags to apply.
       itemList (str,unicode,PyNode or [] of those): The items to tag.

    Returns:
       nothing.

    Raises:
       MayaNodeError - specified node does not exist

    Example usage:
        tagItem('round', 'pSphere1')
        tagItem('round', PyNode('pSphere1'))
        tagItem('round', ['pSphere1','pSphere2'])
        tagItem(['round' 'worldly'], ['pSphere1','pSphere2'])

    TODO: make sure this handles trying to apply same tag more than once.

    """
    if type(itemList) != list:
        itemList = [itemList]
    if type(tagList) != list:
        tagList = [tagList]
    
    tagNodeList = []
    for tag in tagList:
        tagNodeList.append(_getTagNode(tag))
    
    for item in itemList:
        if type(item) == str:
            item = PyNode(item)
        for tagNode in tagNodeList:
            _connectTag(tagNode, item)

def clearTagsOnItem(tagList, itemList, raiseonerror=False, removeEmptyTags=False):
    """Removes one or more tags from one or more items.

    Args:
       tagList (str,unicode,PyNode or [] of those):  The tags to apply.
       itemList (str,unicode,PyNode or [] of those): The items to tag.
    
    kwargs:
       removeEmptyTags(False): will delete any of the tags specified when
           the last item is removed from it.

    Returns:
       nothing.

    Raises:
       MayaNodeError - specified item does not exist
       TypeError     - tag is not on item
       ValueError    - Tag does not exist

    Example usage:
        clearTagsOnItem('round', 'pSphere1')
        clearTagsOnItem('round', PyNode('pSphere1'))
        clearTagsOnItem('round', ['pSphere1','pSphere2'])
        clearTagsOnItem(['round' 'worldly'], ['pSphere1','pSphere2'])

    TODO: Needs to handle tags in namespaces (coming in from referenced objects).
          Probably should add a kwarg to raise if attempt to clear a referenced
          tag from that came in as part of a reference.

    """
    if type(itemList) != list:
        itemList = [itemList]
    if type(tagList) != list:
        tagList = [tagList]
    tagNodeList = []
    for tag in tagList:
        tagNodeList.append(_getTagNode(tag, raiseIfTagDoesntExist=True))
    for item in itemList:
        if type(item) == str:
            item = PyNode(item)
        for tagNode in tagNodeList:
            _disconnectTag(tagNode, item)
    if removeEmptyTags:
        for tagname in tagList:
            if not _listItemsWithTag(tagname):
                delete(_getTagNode(tagname))

def listTags(itemList=None):
    """List tags. Both complete set of defined tags and tags on one or
    more items.  With multiple items, returns the union of the tags on
    the items.

    kwargs:
       itemList (None,str,unicode,PyNode or [] of those): The items to list
       tags on. None for complete set of defined tags.

    Returns:
       set of strings of tag names.

    Raises:
        nothing

    Example usage:
        listTags() - return all tags
        listTags(None) - return all tags
        listTags('pSphere1')
        listTags(PyNode('pSphere1'))
        listTags(['pSphere1','pSphere2'])

    TODO: Needs to handle tags in namespaces (coming in from referenced objects)

    """
    if not itemList:
        return _listTags()
    if type(itemList) != list:
        itemList = [itemList]
    allTags = set()
    for item in itemList:
        allTags.update(_listTagsOnSingleItem(item))
    return allTags

def listMatchingItems(tagList, raiseIfTagDoesntExist=False):
    """List items with the specified list of tags. Items returned have
    all the tags specified.

    Args:
       tagList (str,unicode,PyNode or [] of those):  The tags to apply.

    kwargs:
        raiseIfTagDoesntExist - raises MayaNodeError if set and a specified
            node doesn't exist.  If not set, non-existent tags are silently
            ignored.

    Returns:
       set of items as PyNodes.

    Raises:
        nothing

    Example usage:
        listMatchingItems('round')
        listMatchingItems(['control','round'])

    TODO: Needs to handle tags in namespaces (coming in from referenced objects)

    """
    if type(tagList) != list:
        tagList = [tagList]
    taggedItems = set()
    for tag in tagList:
        newlist = _listItemsWithTag(tag, unreferencedItemsOnly=False)
        if not taggedItems:
            taggedItems = newlist
        else:
            taggedItems = taggedItems.intersection(newlist)
    return taggedItems

def deleteTag(tagList, raiseIfTagDoesntExist=False):
    """Delete the tags.

    Args:
       tagList (str,unicode,PyNode or [] of those):  The tags to apply.

    kwargs:
        raiseIfTagDoesntExist - raises MayaNodeError if set and a specified
            node doesn't exist.  If not set, non-existent tags are silently
            ignored.

    Returns:
       nothing.

    Raises:
        nothing

    Example usage:
        deleteTag('round')
        deleteTag(['control','round'])

    """
    if type(tagList) != list:
        tagList = [tagList]
    for tag in tagList:
        tagNode = _getTagNode(tag, raiseIfTagDoesntExist=raiseIfTagDoesntExist)
        delete(tagNode)

############################################################################################
# Unit Test Area
############################################################################################

import unittest

class TestNodeTagging(unittest.TestCase):
    def setUp(self):
        import maya.mel as mel
        mel.eval('polySphere -name pSphere1 -r 1 -sx 20 -sy 20 -ax 0 1 0 -cuv 2 -ch 1')
        mel.eval('polySphere -name pSphere2 -r 1 -sx 20 -sy 20 -ax 0 1 0 -cuv 2 -ch 1')
        mel.eval('polySphere -name pSphere3 -r 1 -sx 20 -sy 20 -ax 0 1 0 -cuv 2 -ch 1')
        mel.eval('polyCube   -name pCube1 -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1')
        mel.eval('polyCube   -name pCube2 -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1')
        mel.eval('polyCube   -name pCube3 -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1')

    def tearDown(self):
        import maya.mel as mel
        from pymel.all import delete, ls
        mel.eval('delete pCube3')
        mel.eval('delete pCube2')
        mel.eval('delete pCube1')
        mel.eval('delete pSphere3')
        mel.eval('delete pSphere2')
        mel.eval('delete pSphere1')
        delete(ls('tag_*'))

    def test_basicTags(self):
        tagItem('round', 'pSphere1')
        tagItem('round', 'pSphere2')
        tagItem('spherical', 'pSphere2')
        tagItem('foo', 'pSphere2')

        # Do all tags get created?
        tags = sorted(listTags())
        expectedtags = ['foo', 'round', 'spherical']
        self.assertEqual(tags, expectedtags, 'Returned tag list %s != %s' % (tags,expectedtags))

        # Do multiple tagged items get listed?
        roundthings = sorted([str(n) for n in listMatchingItems('round')])
        expectedroundthings = ['pSphere1', 'pSphere2']
        self.assertEqual(roundthings, expectedroundthings, 'Returned round things list %s != %s' % (roundthings,expectedroundthings))

    def test_multiTagging(self):
        tagItem('round', ['pSphere1', 'pSphere2'])
        tagItem(['rotund', 'worldly'], 'pSphere3')

        # Do all tags get created?
        tags = sorted(listTags())
        expectedtags = ['rotund', 'round', 'worldly']
        self.assertEqual(tags, expectedtags, 'Returned tag list %s != %s' % (tags,expectedtags))

        # Do all items get returned?
        items = sorted(listMatchingItems('rotund'))
        for i in items:
            self.assertTrue(isinstance(i, PyNode), 'listMatchingItems(\'rotund\') did not return PyNode for %s'%i)

        expecteditems = ['pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        # Do all items get returned?
        items = sorted(listMatchingItems('round'))
        for i in items:
            self.assertTrue(isinstance(i, PyNode), 'listMatchingItems(\'rotund\') did not return PyNode for %s'%i)

        expecteditems = ['pSphere1', 'pSphere2']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

    def test_multipleTags(self):
        tagItem('round', 'pSphere1')
        tagItem('round', 'pSphere2')
        tagItem('round', 'pSphere3')
        tagItem('spherical', 'pSphere2')
        tagItem('worldly', 'pSphere1')
        tagItem('worldly', 'pSphere3')
        tagItem('foo', 'pCube2')

        items = sorted(listMatchingItems(['spherical','worldly']))
        for i in items:
            self.assertTrue(isinstance(i, PyNode), 'listMatchingItems([\'spherical\',\'worldly\']) did not return PyNode for %s'%i)

        expecteditems = []
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        items = sorted(listMatchingItems(['round','worldly']))
        for i in items:
            self.assertTrue(isinstance(i, PyNode), 'listMatchingItems([\'round\',\'worldly\']) did not return PyNode for %s'%i)

        expecteditems = ['pSphere1', 'pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

    def test_clearingTags(self):
        tagItem('renderablegeo', 'pSphere1')
        tagItem('renderablegeo', 'pSphere2')
        tagItem('renderablegeo', 'pSphere3')
        tagItem('renderablegeo', ['pCube1', 'pCube2', 'pCube3'])
        tagItem('worldly', ['pSphere1','pSphere3'])

        items = sorted(listMatchingItems(['renderablegeo']))
        expecteditems = ['pCube1', 'pCube2', 'pCube3', 'pSphere1', 'pSphere2', 'pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))
        items = sorted(listMatchingItems(['worldly']))
        expecteditems = ['pSphere1', 'pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        clearTagsOnItem(['worldly'], ['pSphere1'])

        items = sorted(listMatchingItems(['renderablegeo']))
        expecteditems = ['pCube1', 'pCube2', 'pCube3', 'pSphere1', 'pSphere2', 'pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        items = sorted(listMatchingItems(['worldly']))
        expecteditems = ['pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        items = sorted(listMatchingItems(['renderablegeo','worldly']))
        expecteditems = ['pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        clearTagsOnItem(['renderablegeo'], ['pCube1'])
        items = sorted(listMatchingItems(['renderablegeo','worldly']))
        expecteditems = ['pSphere3']
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

        clearTagsOnItem(['worldly', 'renderablegeo'], ['pSphere3'])
        items = sorted(listMatchingItems(['renderablegeo','worldly']))
        expecteditems = []   # should be empty set since nothing has worldly tag
        self.assertEqual(items, expecteditems, 'Returned item list %s != %s' % (items,expecteditems))

    def test_tagDeletionOnClear(self):
        tagItem('renderablegeo', 'pSphere1')
        tagItem('renderablegeo', 'pSphere2')
        tagItem('renderablegeo', 'pSphere3')
        tagItem('worldly', ['pSphere1','pSphere3'])

        tags = sorted(listTags())
        expectedTags = ['renderablegeo', 'worldly']
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

        clearTagsOnItem(['renderablegeo'], ['pSphere1'], removeEmptyTags=True)
        clearTagsOnItem(['worldly'], ['pSphere1'], removeEmptyTags=True)
        tags = sorted(listTags())
        expectedTags = ['renderablegeo', 'worldly']
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

        clearTagsOnItem(['renderablegeo'], ['pSphere2', 'pSphere3'], removeEmptyTags=True)
        clearTagsOnItem(['worldly'], ['pSphere3'], removeEmptyTags=True)
        tags = sorted(listTags())
        expectedTags = []
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

    def test_tagDeletion(self):
        tagItem('renderablegeo', 'pSphere1')
        tagItem('renderablegeo', 'pSphere2')
        tagItem('renderablegeo', 'pSphere3')
        tagItem('worldly', ['pSphere1','pSphere3'])

        tags = sorted(listTags())
        expectedTags = ['renderablegeo', 'worldly']
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))
        renderablethings = sorted([str(n) for n in listMatchingItems('renderablegeo')])
        expectedrenderablethings = ['pSphere1', 'pSphere2', 'pSphere3']
        self.assertEqual(renderablethings, expectedrenderablethings, 'Returned round things list %s != %s' % (renderablethings,expectedrenderablethings))

        deleteTag(['worldly'])
        tags = sorted(listTags())
        expectedTags = ['renderablegeo']
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))
        renderablethings = sorted([str(n) for n in listMatchingItems('renderablegeo')])
        expectedrenderablethings = ['pSphere1', 'pSphere2', 'pSphere3']
        self.assertEqual(renderablethings, expectedrenderablethings, 'Returned round things list %s != %s' % (renderablethings,expectedrenderablethings))

        deleteTag(['renderablegeo'])
        tags = sorted(listTags())
        expectedTags = []
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

        
class TestReferenceTagging(unittest.TestCase):
    def setUp(self):
        import os
        import maya.mel as mel
        from pymel.all import scriptEditorInfo
        mel.eval('polySphere -name pLocalSphere1 -r 1 -sx 20 -sy 20 -ax 0 1 0 -cuv 2 -ch 1')
        mel.eval('polySphere -name pLocalSphere2 -r 1 -sx 20 -sy 20 -ax 0 1 0 -cuv 2 -ch 1')
        mel.eval('polySphere -name pLocalSphere3 -r 1 -sx 20 -sy 20 -ax 0 1 0 -cuv 2 -ch 1')
        mel.eval('polyCube   -name pLocalCube1 -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1')
        mel.eval('polyCube   -name pLocalCube2 -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1')
        mel.eval('polyCube   -name pLocalCube3 -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1')

        self._originfosetting = scriptEditorInfo(q=True, si=True)
        scriptEditorInfo(si=True)
        codedir = os.path.split(os.path.abspath(__file__).replace('\\','/'))[0]
        mel.eval('file -r -type "mayaAscii"  -ignoreVersion -gl -mergeNamespacesOnClash false -namespace "tagged" -options "v=0;p=17;f=0" "{}/unittestdata/tagged.ma";'.format(codedir))
        mel.eval('file -r -type "mayaAscii"  -ignoreVersion -gl -mergeNamespacesOnClash false -namespace "untagged" -options "v=0;p=17;f=0" "{}/unittestdata/untagged.ma";'.format(codedir))

    def tearDown(self):
        import maya.mel as mel
        from pymel.all import delete, ls
        from pymel.all import scriptEditorInfo
        mel.eval('file -removeReference -referenceNode "untaggedRN"')
        mel.eval('file -removeReference -referenceNode "taggedRN"')
        mel.eval('delete pLocalCube3')
        mel.eval('delete pLocalCube2')
        mel.eval('delete pLocalCube1')
        mel.eval('delete pLocalSphere3')
        mel.eval('delete pLocalSphere2')
        mel.eval('delete pLocalSphere1')
        delete(ls('tag_*'))
        scriptEditorInfo(si=self._originfosetting)

    def test_referencedTags(self):
        expectedItems = ['tagged:pCube1']
        items = sorted([str(n) for n in listMatchingItems('refnotround')])
        self.assertEqual(items, expectedItems, 'Returned not round things list %s != %s' % (items,expectedItems))

    def test_listingReferencedTags(self):
        expectedTags = ['refnotround', 'refround', 'refsquare']
        tags = sorted([str(n) for n in listTags()])
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

    def test_listingReferencedTagsOnObjects(self):
        expectedTags = ['refnotround', 'refsquare']
        tags = sorted([str(n) for n in listTags('tagged:pCube1')])
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

        tagItem('renderablegeo', 'tagged:pCube1')

        expectedTags = ['refnotround', 'refsquare', 'renderablegeo']
        tags = sorted([str(n) for n in listTags('tagged:pCube1')])
        self.assertEqual(tags, expectedTags, 'Returned tag list %s != %s' % (tags,expectedTags))

    def test_mergingReferencedTags(self):
        tagItem('refnotround', ':pLocalCube1')

        expectedItems = ['pLocalCube1', 'tagged:pCube1']
        items = sorted([str(n) for n in listMatchingItems('refnotround')])
        self.assertEqual(items, expectedItems, 'Returned not round things list %s != %s' % (items,expectedItems))

    def test_taggingReferencedItems(self):
        tagItem('renderablegeo', '|pLocalSphere1')
        tagItem('renderablegeo', '|pLocalSphere2')
        tagItem('renderablegeo', '|pLocalSphere3')
        tagItem('renderablegeo', 'tagged:pSphere1')
        tagItem('renderablegeo', 'tagged:pCube1')
        tagItem('renderablegeo', 'untagged:pSphere1')
        tagItem('renderablegeo', 'untagged:pCube1')

        expectedItems = ['pLocalSphere1', 'pLocalSphere2', 'pLocalSphere3', 'tagged:pCube1', 'tagged:pSphere1', 'untagged:pCube1', 'untagged:pSphere1']
        items = sorted([str(n) for n in listMatchingItems('renderablegeo')])
        self.assertEqual(items, expectedItems, 'Returned renderable things list %s != %s' % (items,expectedItems))

    def test_clearingTagOnReferencedItems(self):
        tagItem('renderablegeo', '|pLocalSphere1')
        tagItem('renderablegeo', '|pLocalSphere2')
        tagItem('renderablegeo', '|pLocalSphere3')
        tagItem('renderablegeo', 'tagged:pSphere1')
        tagItem('renderablegeo', 'tagged:pCube1')
        tagItem('renderablegeo', 'untagged:pSphere1')
        tagItem('renderablegeo', 'untagged:pCube1')

        clearTagsOnItem(['renderablegeo'], ['pLocalSphere1', 'tagged:pSphere1', 'untagged:pSphere1'])

        expectedItems = ['pLocalSphere2', 'pLocalSphere3', 'tagged:pCube1', 'untagged:pCube1']
        items = sorted([str(n) for n in listMatchingItems('renderablegeo')])
        self.assertEqual(items, expectedItems, 'Returned renderable things list %s != %s' % (items,expectedItems))
